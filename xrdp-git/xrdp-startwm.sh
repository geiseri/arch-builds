#!/bin/sh

SESSIONS="openbox-session"
xsessionddir="/etc/X11/Xsession.d"
OPTIONFILE=/etc/X11/Xsession.options
USERXSESSION=$HOME/.xsession
USERXSESSIONRC=$HOME/.xsessionrc
ALTUSERXSESSION=$HOME/.Xsession

if [ -r /etc/locale.conf ]; then
  . /etc/locale.conf
  export LANG LANGUAGE
fi

[ -e /usr/bin/$SESSIONS ] && /usr/bin/$SESSIONS
