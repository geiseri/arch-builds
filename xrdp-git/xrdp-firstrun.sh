#!/bin/sh

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
PIDDIR=/var/run/xrdp
USERID=xrdp
RSAKEYS=/etc/xrdp/rsakeys.ini

# Check for pid dir
if [ ! -d $PIDDIR ] ; then
    mkdir $PIDDIR
fi

chown $USERID:$USERID $PIDDIR

# we need to log to /var/log/xrdp.log
# but we need permission to do so
touch /var/log/xrdp.log
chown $USERID:$USERID /var/log/xrdp.log

# Check for rsa key 
if [ ! -f $RSAKEYS ] || cmp $RSAKEYS /usr/share/doc/xrdp/rsakeys.ini > /dev/null; then
   echo "Generating xrdp RSA keys..."
   (umask 077 ; xrdp-keygen xrdp $RSAKEYS)
   chown $USERID:$USERID $RSAKEYS
   if [ ! -f $RSAKEYS ] ; then
       echo "could not create $RSAKEYS"
       exit 1
   fi
   echo "ready..."
fi
